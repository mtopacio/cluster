# RPI Cluster

This is a short instruction set help setup a bramble, or a Raspberry Pi cluster, and help automate the creation of nodes, both masters and workers. The hardware I am using are a few RPi3 B's, _Samsung_ 64GB SD cards, USBs connected to a power strip, and ethernet wires connected to a hub. 

## sdcard.sh

Run this script to help create your SD cards with either Raspberry Pi OS or ArchLinuxARM. Rasperry Pi OS Lite is the default OS since these will all be headless. `sudo` permissions will probably be required based on your own settings. Being run on Debian, I needed `sudo` to access `dd` and `fdisk`. Run the script with the option `-a` for a full format, including wiping the disk, downloading Raspberry Pi OS, partitioning the disk, creating the filesystem, and burning the OS to disk. In the end, I used the following to install an Arch OS on all my SD cards:

####_NOTE: It is critical you select the right disk as this may result in the loss of data._

```bash
$ sudo ./sdcard.sh -o arch -a
```
---

## Ansible

What we have on the card right now is a base Arch installation, which means basically nothing. Moving forward, I'll just be focusing on an ArchLinuxARM installation. Default username is _alarm_ and the password is _alarm_.

_Ansible_ is a clientless confguration management tool used in DevOps. I chose this versus other frameworks due to the fact that it's clientless. Rather than installing aclient a server would talk to on one of the nodes, _Ansible_ will utilize ssh to execute it's commands. It's built on a number of modules that can help in different ares, like defining users or installing packages.   

To run one of the playbooks, enter the following:

```bash
$ ansible-playbook -i hosts playbooks/[script_name.yml] --ask-vault-pass
```

#### IP Addresses 

This part is not automated since every case is unique. I don't have a lot in my home lab, and I'd prefer it if these nodes IP addresses were organized to reflect their location/order.  

First, I'll usually change the local IP for each of the Pi's as they come onlinei.

I use `nmap` to do a quick scan of all my active IP's and take note. I power on the Pi and connect it to the LAN. I assume the new IP address is my pi and ssh into it. This manual portion is just using ssh to change the local IP. The Ansible playbook will be responsible for further formatting and installing all our requirements. You also do not need this step. It's a personal preference to have my cluster IPs sequential. 
```
$ sudo apt install nmap         # If you don't already have it
$ nmap -sn 192.168.0.1/24       # This may also be different depending in your network admin
                                # This is where you would plug in the Rpi3
$ nmap -sn 192.168.0.1/24       # 2nd Scan. New IP address is your Rpi3
$ ssh pi@192.168.new.IP         

alarm@alarmpi: $ sudo ifconfig eth0 192.168.0.100 netmask 255.255.255.0. up
alarm@alarmpi: $ exit
```
Do the same for '192.168.0.101', '192.168.0.102', and whatever else IPs come up as you introduce mor pi's.

Next, you'll have to add all the IP addresses of your Pi's to your _Ansible_ host file (usually `/etc/ansible/hosts`).

```bash
[cluster_master]
192.168.0.100

[cluster_nodes]
192.168.0.101
192.168.0.102
192.168.0.103
192.168.0.104

[cluster:children]
cluster_master
cluster_nodes
```

#### Configuration

My playbooks are split into a few modules involving the initialization of a node, configuring passwords, and installing the dependencies for the Apache ecosystem. 

`01_init.yml` mostly relies on _Ansible_'s `raw` module since python isn't installed yet. It equates this lack of python as a representation of a new node. It initializes and populates the pacman keyring via `pacman-key init && pacman-key -populate archlinuxarm`. It then updates our version of Arch via `pacman -Syu`. If you haven't use pacman before, `-S` refers to synchronize packages. y` indicates _refresh_, which downloads a copy of the master package databases. `u` indicates a system upgrade, which upgrades all packages that are out-of-date.

`python` and `sudo` are both installed via `pacman -S sudo` and `pacman -S python`. Notice I have not elevated privileges. One of the variables near the top of my file is `become`, `become_user`, and `become_method`. All of this indicate to _Ansible_ I want to become elevate my privileges after logging in with the default `ansible_user` alarm. `ansible_user`, `ansible_ssh_pass`, `ansible_become_pass` are all defined in my host file under `/etc/ansible/hosts`.

I'm also going to set the hostname dependent on the name given the IP in the host file.

Last thing I'm going to do is remove the user `alarm` from the group wheel, which is given sudo privilges in the file `/etc/sudoers` via the task with the line `%wheel ALL=(ALL) NOPASSWD:ALL`. Users within this group will have elevated privileges without the requirment of a password.

Once python is installed, I can start to use the buil-in modules to edit the user configurations. 

`02_passwords.yml` deals with creating a generalized user `hades` for all the nodes. All of these tasks are executed if the user 'hades' isn't found. That user will be added and the root password will be changed. 

`03_apache_dependencies` deals with installing all the necessary packages to run _Apache_ _Spark_.

#### Hadoop






