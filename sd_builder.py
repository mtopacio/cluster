import os
import requests
from curses import wrapper
import curses

screen = curses.initscr()
curses.start_color()
num_rows,num_cols = screen.getmaxyx()

# id, foreground, background
curses.init_pair(1,curses.COLOR_BLACK,curses.COLOR_WHITE)   # main
curses.init_pair(2,curses.COLOR_WHITE,curses.COLOR_BLACK)   # shadow
curses.init_pair(3,curses.COLOR_WHITE,curses.COLOR_YELLOW)  # background
curses.init_pair(4,curses.COLOR_BLUE,curses.COLOR_WHITE)    # title

def background():

    # colored background
    screen.bkgd(' ', curses.color_pair(3))
    screen.refresh()

    # box on background
    box_width = 80
    box_height = 20
    box_top = int(num_rows/2) - int(box_height/2)
    box_bottom = int(num_rows/2) + int(box_height/2)
    box_left = int(num_cols/2) - int(box_width/2)


    # shadow for box
    shadow_window = curses.newwin(box_height, box_width, box_top+1, box_left+2)
    shadow_window.bkgd(' ', curses.color_pair(2))
    shadow_window.refresh()

    # text box
    window = curses.newwin(box_height, box_width, box_top, box_left)
    window.bkgd(' ', curses.color_pair(1))
    window.box()
    window.refresh()

    return window

def menu_screen(win):

    win.clear()
    options = [ "All",
                "Download OS",
                "Wipe - overwrite disk with zeros",
                "Partition",
                "Write OS to disk",
                "Exit"]

    win.addstr(0,0,"Select task:")
    return selection(win, options, 0, 0)

def download_os(win, interact = True):

    win.clear()
    w_rows, w_cols = win.getmaxyx()
    win.addstr(0,0,"Download OS - Select one:")
    options = { "RaspbianLite":"https://downloads.raspberrypi.org/raspios_lite_armhf_latest",
                "ArchARM":"http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-aarch64-latest.tar.gz"}
    choice = selection(win, list(options.keys()), 0,0)

    os = list(options.keys())[choice]
    url = options[os]
    filename = url.split('/')[-1]

    win.addstr(w_rows-3, 0, f"Downloading...")
    win.refresh()

    with open(filename, 'wb') as f:
        response = requests.get(url, stream=True)
        total_length = response.headers.get('content-length')
        if total_length is None:
            f.write(response.raw.read())
        else:
            dl = 0
            total_length=int(total_length)
            win.addstr(w_rows-4, 0, f"{filename}")
            for data in response.iter_content(chunk_size=4096):
                dl+=len(data)
                f.write(data)
                done = dl/total_length
                percent = f"{int(done*100)}%{' '*(3 - len(str(done)))}"
                bar_width = int(0.9 * w_cols)
                done_bar = f"{'=' * int(done*bar_width)}"
                pending_bar = f"{' '* (bar_width-len(done_bar))}"
                win.addstr(w_rows-3,0,f"{percent} |{done_bar}{pending_bar}|")
                win.refresh()


def wipe(win):
    return "Wipe"

def partition(win):
    return "Partition"

def write(win):
    return "Write"

def all_tasks(win):
    return "all tasks"


def main(main_screen):

    win = background()

    # title
    win.addstr(1,2, "SD CARD UTILITY - Create SD cards for RPi Cluster", curses.A_BOLD)
    win.refresh()

    # text window that will be constantly updated
    w_rows, w_cols = win.getmaxyx()
    text_win = win.derwin(w_rows-4,w_cols-8,3,4)
    text_win.keypad(1)

    while True:

        curses.color_pair(1)

        choice = menu_screen(text_win)

        menu = {    0 : all_tasks,
                    1 : download_os,
                    2 : wipe,
                    3 : partition,
                    4 : write,
                    5 : exit}

        #text_win.addstr(14,0, f"Selected: {menu[choice]}")
        #text_win.refresh()
        screen.refresh()
        #curses.napms(1000)
        menu[choice](text_win)
    curses.endwin()

def selection(win, options, y, x):

    selected = 0

    while True:
        for i,opt in enumerate(options):
            if i == selected:
                box = 'X'
            else:
                box = ' '
            win.addstr(y+i+2, x+2, f"[{box}] {opt}")
            win.refresh()

        k = win.getch()
        if k == curses.KEY_UP and selected != 0:
            selected-=1
        elif k == curses.KEY_DOWN and selected < len(options)-1:
            selected+=1
        elif k == 10:
            break

    return selected

if __name__=="__main__":
    wrapper(main)

