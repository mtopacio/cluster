# Cluster Playbooks

Only `main.yml` is meant to be run periodically. Operations are segmented into different file, each with a task te determine the current state of affairs. Running the entire playbook should not break anything, only configure everything appropriately to be on par with the rest of the nodes. 

## Ansible Vault
Ansible vault is another product used to create a hash for stored passwords.

```bash
$ EDITOR=vim ansible-vault create vault.yml     # new vault file using vim editor
$ EDITOR=vim ansible-vault edit vault.yml       # edit vault file using vim editor
$ ansible-vault view vault.yml                  # view key:values; prompts password
```
You can encrypt multiple files at once

```bash
$ ansible-vault encrypt file1.yml file2.yml file3.yml
```


## Commands
```bash
$ ansible-playbook [script.yml] --ask-vault-pass
```
