#!/usr/bin/env bash

# Purpose: Script for formatting SD card for RPI3 Cluster
# By: Mark Topacio

# Note: There are a few random calls to 'sleep' to deal with lagging I/O

# defaults
CURRENT_VERSION='buster'
IGNORE_CONFIRMS=false
SET_OS='raspios'

# text styles
NORMAL="\e[0m"
BOLD="\e[1m"
RED="\e[31m"
GREEN="\e[32m"

usage() {
    echo "Usage: $0 [OPTIONS]"
    echo 
    echo "Run with one of the following options:"
    echo -e "  -a\t\t\tRun the entire pipe"
    echo -e "  -y\t\t\tAnswer 'Yes' for all confirmations"
    echo -e "  -p \path\\\to\disk\tSet the path to the correct disk"
    echo -e "  -d [raspios|arch]\tDownload Raspberry Pi OS Lite or ArchLinuxARM (default: raspios)"
    echo -e "  -w\t\t\tWipe SD card"
    echo -e "  -f\t\t\tFormat SD card for burn"
    echo -e "  -b\t\t\tBurn disc image"
    echo -e "  -h\t\t\tPrint help menu."
    echo
    echo "Sources:"
    echo "https://www.raspberrypi.org/documentation/installation/installing-images/"
    echo "https://marktopac.io/git/mtopacio/cluster"

    exit 1
}

ignore_confirms() {
    IGNORE_CONFIRMS=true
}

set_drive() {
    DRIVE_PATH=$1
}

set_os() {
    SET_OS=$1
}

dependency_check() {
    DEPENDENCIES=(wget fdisk dd unzip sha256sum md5sum bsdtar)
    DEPENDENCIES_MET=true

    for DEP in ${DEPENDENCIES[@]}; do
        if ! [[ -x "$(command -v $DEP)" ]]; then
            echo -e "${BOLD}${DEP}${NORMAL} not found"
            DEPENDENCIES_MET=false
        fi
    done

    if ! [[ $DEPENDENCIES_MET ]]; then
        echo Please install the required dependencies and try again.
        exit 1
    fi
}

confirm() {
    if ! $IGNORE_CONFIRMS; then
        read -p "Are you sure you want to continue? (y/n) " -n 1 -r
        echo
        if [[ ! $REPLY =~ ^[Yy]$ ]]
        then
            echo
            echo $0 was cancelled
            exit 1 
        fi
    fi
}

drive() {
    DRIVE=$(lsblk -l -p -o NAME,MODEL,SIZE,TRAN | grep usb)
    if [[ ${#DRIVE[@]} == 1 ]]; then
        DRIVE_PATH=$(echo $DRIVE | cut -d' ' -f1)
        DRIVE_MODEL=$(echo $DRIVE | cut -d' ' -f2)
        DRIVE_SIZE=$(echo $DRIVE | cut -d' ' -f3)
        DRIVE_TYPE=$(echo $DRIVE | cut -d' ' -f4)
        echo A ${DRIVE_SIZE} ${DRIVE_MODEL} ${DRIVE_TYPE^^} was found on ${DRIVE_PATH}.
        echo This will be used as the drive to write to.
        echo
        echo -e ${RED}Make sure the correct drive is selected. Any data WILL be overwritten.
        echo -e DO NOT INTERRUPT OR YOUR STORAGE DEVICE MAY BE BRICKED.${NORMAL}
    elif [ ${#DRIVE[@]} == 0 ]; then
        echo No drives were found with removable storage. Please use the option -p [path/to/disk].
    else    
        echo Multiple drives were found with removable storage. Please use the option -p [path/to/disk].
    fi
    echo
}

get_os() {
    if [[ ${SET_OS} == "arch" ]]; then 

        echo -e "${BOLD}ArchLinuxARM${NORMAL} selected as OS."
        if ! [[ -e ArchLinuxARM-rpi-2-latest.tar.gz ]]; then
            wget http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-2-latest.tar.gz
        fi
        if [[ -e ArchLinuxARM-rpi-2-latest.tar.gz ]]; then
            echo Checking MD5SUM for archived ArchLinuxARM OS...
            CHECK=$(wget http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-2-latest.tar.gz.md5 -q -O - | md5sum -c)
            echo ${CHECK}
            if ! [[ $(echo ${CHECK} | cut -d' ' -f2) = "OK" ]]; then
                echo "MD5UM DOES NOT MATCH THE ONE PULLED FROM ARLINUXARM.ORG"
                echo "This script may be pulling checksum. Investigate to avoid any"
                echo "man-in-the-middle attacks. If alright, proceed with caution."
                echo
                confirm
            fi
        fi
    else

        echo -e "${BOLD}Raspberry Pi OS${NORMAL} selected as OS."
        BASE_URL=""https://downloads.raspberrypi.org/raspios_lite_armhf 
        RELEASE_DATE=$(wget ${BASE_URL}/release_notes.txt -q -O - | head -n 1 | cut -d':' -f1)
        LATEST_FOLDER=$(wget ${BASE_URL}/images -q -O - | grep 'href="raspios_lite_armhf' | cut -d'>' -f7 | cut -d'/' -f1 | tail -n 1)
        RELEASE_NAME="${RELEASE_DATE}-raspios-buster-armhf-lite.zip"  
        
        if ! [[ -e ${RELEASE_NAME} || -e "$(echo ${RELEASE_NAME} | cut -d'.' -f1).img" ]]; then
            echo "Raspberry Pi OS not found in local directory. Downloading latest version... "
            wget ${BASE_URL}/images/${LATEST_FOLDER}/${RELEASE_NAME}
        elif [[ -e ${RELEASE_NAME} ]]; then
            echo "Local zipped file found."
        elif [[ -e "$(echo ${RELEASE_NAME} | cut -d'.' -f1).img" ]]; then
            echo "Local disk image found."
        fi
        if [[ -e ${RELEASE_NAME} ]]; then

            # This part gets a little tricky. I'm trying to do this without the use of anything but bash.
            # It seems easier to just go to downloads.raspberrypi.org and find the link for the version verus
            # trying to parse the raspberrypi.org/software/operating systems. There are multiple SHA-256 hases
            # on there. 

            echo "Checking SHA256SUM for compressed Raspberry Pi OS..."

            SHA256_HASH=$(wget https://downloads.raspberrypi.org/raspios_lite_armhf/images/${LATEST_FOLDER}/${RELEASE_NAME}.sha256 -q -O -)

            CHECK=$(echo ${SHA256_HASH} | sha256sum --check)
            echo ${CHECK}
            if ! [[ $(echo ${CHECK} | cut -d' ' -f2) = "OK" ]]; then
                echo "SHA256SUM DOES NOT MATCH THE ONE PULLED FROM RASPBERRYPI.ORG"
                echo "This script may be pulling checksum. Investigate to avoid any"
                echo "man-in-the-middle attacks. If alright, proceed with caution."
                echo
                confirm
            fi
        
            echo Unzipping raspbian_lite_latest...
            unzip ${RELEASE_NAME} 
            echo Deleting compressed file...
            rm ${RELEASE_NAME} 
        else
            echo "Download failed."
            exit 1
        fi
    fi
}


burn() {

    if [[ ${SET_OS} == 'arch' ]]; then
        echo Writing ArchLinuxARM to selected drive...
        echo Making temporary directories
        mkdir boot
        mkdir root
        echo Mounting disk to local directories
        mount "${DRIVE_PATH}"1 boot
        mount "${DRIVE_PATH}"2 root
        sleep 2
        echo Copying files to mounted directories
        su -c "bsdtar -xpf ArchLinuxARM-rpi-2-latest.tar.gz -C root" $(whoami) 
        sync
        su -c "mv root/boot/* boot" $(whoami)
        sleep 2
        echo Unmounting directories
        umount boot root
        echo Removing temporary directories
        rmdir boot
        rmdir root
    else
        echo -e "Writing Raspberry Pi OS to selected drive..."
        dd bs=1M status=progress if=$(ls | grep *raspiosi*lite.zip) of=$1 conv=fsync
        
        # ssh is disabled by default
        mkdir tmp
        mount "${1}1" tmp/
        cd tmp/
        touch ssh
        if [ -e ssh ]; then
            echo ssh file successfully created.
        else
            echo ssh file could not be created.
        fi
        cd ..
        sleep 2 # keeps happening too fast. ssh file not created before trying to unmount
        umount tmp
        rmdir tmp
    fi
}

wipe() {
    echo "Process will completely wipe the SD card, using zeros to write over any data."
    confirm
    echo "Deleting Data. This may take a while..."
    dd status=progress if=/dev/zero of=${DRIVE_PATH} bs=8192
}

format() {
    echo -e "Partitioning SD card and creating filesystem..."
    if [[ ${SET_OS} == "arch" ]]; then
        echo Partitioning for ArchLinuxARM... 
        # https://archlinuxarm.org/platforms/armv6/raspberry-pi
        parted --script ${DRIVE_PATH} \
            mklabel msdos \
            mkpart primary fat32 1MiB 200MiB \ 
            set 1 boot on
        sleep 2
        parted --script ${DRIVE_PATH} \
            mkpart primary ext4 200MiB 100% 
        sleep 2
        mkfs.vfat "${DRIVE_PATH}"1
        mkfs.ext4 "${DRIVE_PATH}"2
        sleep 2
    else
        echo Partitioning for Raspberry Pi OS...
        parted --script ${DRIVE_PATH} \
            mklabel msdos \
            mkpart primary fat32 1MiB 100% \
            set 1 boot on
        mkfs.vfat "${DRIVE_PATH}1"
    fi    
    sleep 2
}

run_all() { 
    
    echo Running script. This will wipe ${DRIVE_PATH}, download the OS, 
    echo partition the disk, create the filesystem, and write the OS to 
    echo -e "disk. Please verify drive path (${BOLD}${DRIVE_PATH}${NORMAL}) is set correctly."
    confirm

    IGNORE_CONFIRMS=true
    
    wipe
    get_os
    format
    burn
    close
}

close() {
    if [[ ${SET_OS} == "arch" ]]; then
        echo Login as the default user "alarm" with the password "alarm"
        echo The default root password is "root"
    fi
    
}

main() {

    dependency_check
    drive

    while getopts "p:bwdfayo:h" OPTION; do
        case $OPTION in
            b)  burn;;
            w)  wipe;;
            f)  format;;
            p)  set_drive $OPTARG;;
            d)  get_os;;
            o)  set_os $OPTARG;;
            y)  ignore_confirms;;
            a)  run_all;;
            h)  usage;;
            *)  usage;;
        esac
    done
}

main $@





